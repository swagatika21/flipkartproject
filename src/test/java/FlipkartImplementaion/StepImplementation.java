package FlipkartImplementaion;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import junit.framework.Assert;
import pageObjects.Loginpage;
import pageObjects.ProductDescriptionpage;
import pageObjects.Searchpage;
import resources.base;

public class StepImplementation extends base{

	public WebDriver driver;
	@BeforeTest
	public void initialize() throws IOException
	{
		 driver =initializeDriver();
		 driver.get(prop.getProperty("url"));
			
	}
	
		@Test(dataProvider="getData")
		public void LoginPageNavigation(String Username,String Password) throws IOException
		{	
				
			Loginpage lp=new Loginpage(driver);
			lp.getmail().sendKeys(Username);
			lp.getpassword().sendKeys(Password);
			lp.Signin().click();
			//lp.searchquery("Camera");

		}
		
		@Test
		 public void searchCamera() throws IOException
		  { 
			 Searchpage s=new Searchpage(driver);
			 s.searchquery("Camera");
			 Assert.assertEquals(24, s.countofresults());
			 s.countofresults();
			 System.out.println(s.countofresults());
			 s.scrolldown();
			 s.clickitem();
			
			// s.productheader();
			  
		}
		
		@Test
		 public void ProductDescription() throws IOException, InterruptedException
		  { 
			
			
			ProductDescriptionpage p=new ProductDescriptionpage(driver);
			//System.out.println(p.Productdetails());
			
			//p.scrolldown();
			p.Addtocartitem();
			//System.out.println(p.Carttdetails());
			//Assert.assertEquals( p.Productdetails(), p.Carttdetails());
		  }
	
	@AfterClass
		 public void closeBrowser() throws InterruptedException
		 {
		  Thread.sleep(2000);
		  driver.close();
		  driver.quit();
		 }
		 

		@DataProvider
		public Object[][] getData()
		{
			// Row stands for how many different data types test should run
			//coloumn stands for how many values per each test
			
			// Array size is 2
			// 0,1
			Object[][] data=new Object[2][2];
			//0th row with Non valid Data
			data[0][0]="6361166590";
			data[0][1]="liti1234";
			
			//1st row with valid data
			/*data[1][0]="swag@gmail.com";
			data[1][1]="456788";*/
			
			
			return data;
			
					
		}

}
