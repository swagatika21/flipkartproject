package testSteps;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.Loginpage;
import pageObjects.ProductDescriptionpage;
import pageObjects.Searchpage;
import utilities.BaseClass;
import utilities.Helperlib;

public class ProductDescriptionTest extends BaseClass{

	@BeforeTest
	public void setup()
	{	
		
		Loginpage lp=PageFactory.initElements(driver,Loginpage.class);
		lp.login(prop.getProperty("unm"),prop.getProperty("pswd"));
		Searchpage sp=PageFactory.initElements(driver,Searchpage.class);
		sp.searchquery("Camera");
		sp.scrolldown();
		sp.clickitem();
	}
	
	
	@Test()
	public void ProdDetails() throws InterruptedException
	{	
		ProductDescriptionpage pd=new ProductDescriptionpage(driver);
		pd.Addtocartitem();
		pd.Carttdetails();
		
		Helperlib.Takescreenshot(driver, "ProductDescriptionTest");
	}
	
}

