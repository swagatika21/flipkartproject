package testSteps;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pageObjects.Loginpage;
import utilities.BaseClass;
import utilities.ExcelData;
import utilities.Helperlib;

public class loginTest extends BaseClass{

	
	@Test()
	public void LoginApp()
	{	
		//ExcelData excel=new ExcelData();
		//String unm=excel.getstringData("sheet", 1, 1);
	//	String pswd=excel.getstringData("sheet", 1, 1);
		Loginpage lp=PageFactory.initElements(driver,Loginpage.class);
		lp.login(prop.getProperty("unm"),prop.getProperty("pswd"));
		
		Helperlib.Takescreenshot(driver, "loginTest");
	}
	
}
