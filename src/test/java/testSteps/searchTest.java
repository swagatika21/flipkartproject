package testSteps;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.Loginpage;
import pageObjects.ProductDescriptionpage;
import pageObjects.Searchpage;
import utilities.BaseClass;
import utilities.Helperlib;

public class searchTest extends BaseClass {
	public Searchpage sp;
	public Loginpage lp;
	

	
	@BeforeMethod
	public void setup()
	{	
		
		Loginpage lp=PageFactory.initElements(driver,Loginpage.class);
		lp.login(prop.getProperty("unm"),prop.getProperty("pswd"));
	}
	@Test
	public void searchitem() {
		Searchpage sp=PageFactory.initElements(driver,Searchpage.class);
		sp.searchquery("Camera");
		//sp.scrolldown();
		sp.clickitem();
		
		Helperlib.Takescreenshot(driver, "searchquery");
	}
	@Test
	public void ProdDetails() throws InterruptedException
	{	
		ProductDescriptionpage pd=new ProductDescriptionpage(driver);
		pd.Addtocartitem();
		pd.Carttdetails();
		
		Helperlib.Takescreenshot(driver, "Addtocartitem");
	}
	
}
