package pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Searchpage {

	public WebDriver driver;

	public Searchpage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@name='q']") // "//input[@placeholder='Search for products, brands and more']")
	WebElement search;
	@FindBy(xpath = "//a[@class='_31qSD5']")
	List<WebElement> results;

	public int countofresults() {
		return results.size();
	}

	public void scrolldown() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,350)", "");
	}

	public void clickusingjavascript(WebElement element, WebDriver driver) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click();", element);
	}

	public void searchquery(String searchitem) {

		search.sendKeys(searchitem + Keys.ENTER);
	}

	public void clickitem() {
		String img = "3";
		WebElement searchimg = driver.findElement(By.xpath(
				"/html/body/div/div/div[3]/div[2]/div[1]/div[2]/div[" + img + "]/div/div/div/a/div[2]/div[1]/div[1]"));
		searchimg.click();
		searchimg.getText();

	}
	/*
	 * public String productheader() { JavascriptExecutor jse =
	 * (JavascriptExecutor)driver; String TitleName =
	 * jse.executeScript("return document.title;").toString(); //
	 * System.out.println("Title of the page = "+TitleName); return TitleName; wait
	 * = new WebDriverWait(driver, 40);
	 * wait.until(ExpectedConditions.elementToBeClickable(search)).sendKeys(
	 * searchitem+Keys.ENTER); }
	 */
}
