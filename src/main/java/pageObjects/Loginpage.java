package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Loginpage {

public WebDriver driver;
//Constructor
	public Loginpage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//LoginDetails
	@FindBy(xpath=("//input[@class='_2zrpKA _1dBPDZ' and @type='text']"))
	WebElement uname;
	@FindBy(xpath=("//input[@type=\"password\"]"))
	WebElement pass;
	@FindBy(xpath=("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[3]/button[1]"))
	//("//div[3]//button[1]");
	WebElement Login;
	
	//Search Item
	@FindBy (xpath="//input[@name='q']")
	WebElement search;
		
	public WebElement Search() {
		return search;
	}
	public void login(String usernm,String paswd) {
		uname.sendKeys(usernm);
		pass.sendKeys(paswd);
		Login.click();
		
	}

}
