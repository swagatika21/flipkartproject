package pageObjects;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductDescriptionpage {

public WebDriver driver;
public WebDriverWait wait;
	public ProductDescriptionpage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);		
	}

	//Product page
	@FindBy (xpath=("//div[@id='container']/div/div/div/div/div[2]/div[1]/div[1]"))
	WebElement detailpage;
	//Click Add to cart btn
	@FindBy(xpath= "//button[@class='_2AkmmA _2Npkh4 _2MWPVK']")//button[text()='GO TO CART']")and contains(.,'ADD TO CART')
	WebElement Addtocart;
	//cart page     
	@FindBy(xpath=("//a[contains(@class, '_325-ji _3ROAwx')]"))
	WebElement cartpage;	
		
		public String Productdetails() {
			
			String actual=detailpage.getText();
			return actual.trim();
		}
		
		public void Addtocartitem() throws InterruptedException {
			//jse.executeScript("window.scrollBy(0,350)", "");
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView();", Addtocart);
			Set<String> winHandleBefore = driver.getWindowHandles();
			for( String windowHandle:winHandleBefore){
				driver.switchTo().window(windowHandle);}
			wait = new WebDriverWait(driver, 40);
			WebElement element=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='_2AkmmA _2Npkh4 _2MWPVK']")));
			element.click();
			//wait.until(ExpectedConditions.elementToBeClickable(Addtocart)).click();
			//return Addtocart;
		}
		//cart detail page
		public String Carttdetails() {
			
			String expected=cartpage.getText();
			return expected.trim();
		}


}
