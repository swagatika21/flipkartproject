package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelData {
	XSSFWorkbook wb;
	
	public ExcelData()  {
		File src= new File(System.getProperty("user.dir")+"/TestData/Datasheet.xlsx");
		//File src=new File("C:\\Users\\swaga\\eclipse-workspace\\FlipkartProject\\TestData\\TestData.xlsx");//./TestData/TestData.xlsx");
		try {
			FileInputStream fis=new FileInputStream(src);
			
				XSSFWorkbook wb=new XSSFWorkbook(fis);
			} catch (Exception e) {
				System.out.println("Unable to read Excel file"+e.getMessage());
			}
		}
	public String getstringData(String sheetName,int row,int col)
	{
		return wb.getSheet(sheetName).getRow(row).getCell(col).getStringCellValue();
			
	}
	public double getnumericdata(String sheetName,int row,int col)
	{
		return wb.getSheet(sheetName).getRow(row).getCell(col).getNumericCellValue();
	}
	

}
