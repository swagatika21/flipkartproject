package utilities;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class BaseClass extends BrowserFactory{
	public WebDriver driver;
	public ExcelData excel;
	
	@BeforeSuite
	public void excelsetup()
	{
	 excel=new ExcelData();
	}
	@BeforeClass
	public void initialize() throws IOException
	{
		 driver =initializeDriver();
		 driver.get(prop.getProperty("url"));
			
	}
	@AfterClass
	 public void closeBrowser() throws InterruptedException
	 {
	  Thread.sleep(2000);
	  driver.close();
	  driver.quit();
	 }
	 @AfterMethod
	 public void screenshot(ITestResult result)
	 {
		 if(result.getStatus()==ITestResult.FAILURE) {
			 Helperlib.Takescreenshot(driver, "loginTest");
		 }
	 }

}
