package utilities;
import java.io.File;
import java.util.logging.FileHandler;

import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Helperlib {

	

	//Screenshot,Handel alerts,Frame,Window,sync,Javascript executer
	public static void Takescreenshot(WebDriver driver,String screenshotName)
	{
		File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		File Dest=new File("./Screenshot/"+screenshotName+System.currentTimeMillis()+".png");
		//File dest=new File("./Screenshots/Login.png");
		
		try {
			
			FileUtils.copyFile(src, Dest);
		
			System.out.println("Screenshot Captured");
						 
		} catch (Exception e) {
			
			System.out.println("Exception while taking Screesnhot"+e.getMessage());
		}
	}
}
